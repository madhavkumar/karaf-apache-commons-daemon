@echo off

set "APP_HOME=%~dp0"
rem ::Does string have a trailing slash? if so remove it
IF %APP_HOME:~-1%==\ SET "APP_HOME=%APP_HOME:~0,-1%"

set "CURRENT_DIR=%~dp0"
set "CAT_HOME=%~dp0"
set "CAT_BASE=%CAT_HOME%"
set "KARAF_HOME=%APP_HOME%"
set "KARAF_BASE=%APP_HOME%"
set "KARAF_DATA=%CAT_HOME%data"
set "KARAF_ETC=%CAT_HOME%etc"

set SERVICE_NAME=KARAF
set "PR_DISPLAYNAME=Karaf Daemon Service"
set "PR_DESCRIPTION=Karaf Daemon Service."
rem Karaf Main Class
set START_CLASS=org.apache.karaf.main.Main
rem Karaf Stop Class
set STOP_CLASS=org.apache.karaf.main.Stop
set START_METHOD=main
set STOP_METHOD=main
set "PR_LOGPATH=%APP_HOME%\Logs"
set "PR_CONSOLE_LOG=%PR_LOGPATH%\console.log"
set "PR_LOGLEVEL=DEBUG"
set "PR_STOPTIMEOUT=10"
rem disable stdout and stderr logs from daemon
 set "PR_STDOUTPUT=auto"
 set "PR_STDERROR=auto"
set "PR_JVMMX=256"
set "PR_JVMMS=256"
set "DEFAULT_JRE=auto"
set "DEFAULT_JVM=auto"
set "PR_JRE=%DEFAULT_JRE%"
set "PR_JVM=%DEFAULT_JVM%"

if exist "%APP_HOME%\KARAF.exe" goto okHome
echo The KARAF.exe was not found...
goto end

:okHome
rem Make sure prerequisite environment variables are set
if not "%JAVA_HOME%" == "" goto gotJdkHome
if not "%JRE_HOME%" == "" goto gotJreHome
echo Neither the JAVA_HOME nor the JRE_HOME environment variable is defined
echo Service will try to guess them from the registry.
goto okJavaHome
:gotJreHome
if not exist "%JRE_HOME%\bin\java.exe" goto noJavaHome
if not exist "%JRE_HOME%\bin\javaw.exe" goto noJavaHome
if exist "%JRE_HOME%\bin\server\jvm.dll" (
set "PR_JRE=%JRE_HOME%"
set "PR_JVM=%JRE_HOME%\bin\server\jvm.dll"
)
goto okJavaHome
:gotJdkHome
if not exist "%JAVA_HOME%\jre\bin\java.exe" goto noJavaHome
if not exist "%JAVA_HOME%\jre\bin\javaw.exe" goto noJavaHome
if not exist "%JAVA_HOME%\bin\javac.exe" goto noJavaHome
if not "%JRE_HOME%" == "" goto okJavaHome
set "JRE_HOME=%JAVA_HOME%\jre"
if exist "%JRE_HOME%\bin\server\jvm.dll" (
set "PR_JRE=%JRE_HOME%"
set "PR_JVM=%JRE_HOME%\bin\server\jvm.dll"
)
goto okJavaHome
:noJavaHome
echo The JAVA_HOME environment variable is not defined correctly
echo This environment variable is needed to run this program
echo NB: JAVA_HOME should point to a JDK not a JRE
goto end
:okJavaHome
if not "%CAT_BASE%" == "" goto gotBase
set "CAT_BASE=%CAT_HOME%"


:gotBase
echo Using CLASSPATH:         "%CLASSPATH%"
set "EXECUTABLE=%APP_HOME%\KARAF.exe"

rem Check if needed.
set PR_CLASSPATH=%CLASSPATH%;%APP_HOME%;%APP_HOME%\lib\boot\org.apache.karaf.main-4.0.8.jar;%APP_HOME%\lib\boot\org.osgi.core-6.0.0.jar;%APP_HOME%\lib\boot\org.apache.karaf.jaas.boot-4.0.8.jar;%APP_HOME%\lib\boot\org.apache.karaf.diagnostic.boot-4.0.8.jar;%APP_HOME%\Config



echo Installing the service '%SERVICE_NAME%' ...
echo Using APP_HOME :		"%APP_HOME%"
echo Using CAT_HOME:    "%CAT_HOME%"
echo Using CAT_BASE:    "%CAT_BASE%"
echo Using JAVA_HOME:        "%JAVA_HOME%"
echo Using JRE_HOME:         "%JRE_HOME%"
echo Using PR_CLASSPATH:         "%PR_CLASSPATH%"
echo Using KARAF_HOME:         "%KARAF_HOME%"
echo Using KARAF_BASE:         "%KARAF_BASE%"
echo Using KARAF_DATA:         "%KARAF_DATA%"
echo Using KARAF_ETC:         "%KARAF_ETC%"
echo Using DEFAULT_JRE:         "%DEFAULT_JRE%"

if not exist "%PR_LOGPATH%" MD "%PR_LOGPATH%"

"%EXECUTABLE%" //IS//%SERVICE_NAME% --Jvm="%PR_JVM%" --DisplayName "%PR_DISPLAYNAME%" --Description "%PR_DESCRIPTION%"^
 --LogPath "%PR_LOGPATH%" ^
 --Startup=auto --StartMode=jvm --StopMode=jvm ^
 --StartClass %START_CLASS% --StopClass %STOP_CLASS% ^
 --StartPath "%APP_HOME%" --StopPath "%APP_HOME%" --StartMethod %START_METHOD% --StopMethod %STOP_METHOD% --Classpath "%PR_CLASSPATH%" ^
 ++JvmOptions="-Dkaraf.base=%KARAF_BASE%"^
 ++JvmOptions="-Dkaraf.data=%KARAF_DATA%"^
 ++JvmOptions="-Dkaraf.etc=%KARAF_ETC%"^
 ++JvmOptions="-Dkaraf.home=%KARAF_HOME%"^
 ++JvmOptions="-Dcom.sun.management.jmxremote"^
 ++JvmOptions="-Dkaraf.startLocalConsole=false"^
 ++JvmOptions="-Dkaraf.startRemoteShell=true"^
 ++JvmOptions="-Djava.endorsed.dirs=%DEFAULT_JRE%jre\lib\endorsed:%KARAF_HOME%\lib\endorsed"^
 ++JvmOptions="-Djava.ext.dirs=%DEFAULT_JRE%jre\lib\ext:%KARAF_HOME%\lib\ext" ^


echo "%EXECUTABLE%" //IS//%SERVICE_NAME% --Jvm="%PR_JVM%" --DisplayName "%PR_DISPLAYNAME%" --Description "%PR_DESCRIPTION%"^
 --LogPath "%PR_LOGPATH%" ^
 --Startup=auto --StartMode=jvm --StopMode=jvm^
 --StartClass %START_CLASS% --StopClass %STOP_CLASS%^
 --StartPath "%APP_HOME%" --StopPath "%APP_HOME%" --StartMethod %START_METHOD% --StopMethod %STOP_METHOD% --Classpath "%PR_CLASSPATH%" ^
 ++JvmOptions="-Dkaraf.base=%KARAF_BASE%"^
 ++JvmOptions="-Dkaraf.data=%KARAF_DATA%"^
 ++JvmOptions="-Dkaraf.etc=%KARAF_ETC%"^
 ++JvmOptions="-Dkaraf.home=%KARAF_HOME%"^
 ++JvmOptions="-Dcom.sun.management.jmxremote"^
 ++JvmOptions="-Dkaraf.startLocalConsole=false"^
 ++JvmOptions="-Dkaraf.startRemoteShell=true"^
 ++JvmOptions="-Djava.endorsed.dirs=%KARAF_HOME%\lib\endorsed"^
 ++JvmOptions="-Djava.ext.dirs=%DEFAULT_JRE%jre\lib\ext" ^
 

if not errorlevel 1 goto installed
echo Failed installing '%SERVICE_NAME%'-ERROR %errorlevel% service
goto end

:installed
rem Clear the environment variables. They are not needed any more.
sc failure %SERVICE_NAME% reset= 0 actions= restart/60000
echo Service Installed Successfully ############################
net start %SERVICE_NAME%



:end
cd "%CURRENT_DIR%"
pause

GOTO :EOF

:RESOLVE
SET %2=%~f1
GOTO :EOF
